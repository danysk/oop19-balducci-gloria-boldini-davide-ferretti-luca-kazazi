package other;

public interface PairInterface {

    @Override
    int hashCode();

    @Override
    boolean equals(Object obj);

    @Override
    String toString();

}
