package controllers.timer;

public interface GeneralTimerInterface {

    /**
     * method to start the timer.
     */
    void start();

    /**
     * method to stop the timer.
     */
    void stop();

}
